# Aplicación VueJS y Symfony

Este proyecto contiene una aplicación VueJS para el frontend y una aplicación Symfony para el backend.
La aplicación VueJS se encuentra dentro de la carpeta frontend

## Requisitos

- Node.js
- npm (Node Package Manager)
- PHP
- Composer
- PostgreSQL (u otro motor de base de datos compatible con Symfony)

## Instalación

1. **Clonar el repositorio**

    ```bash
    git clone https://gitlab.com/elyohan14/money-converter
    cd money-converter
    ```

2. **Instalar dependencias del frontend (Quasar)**

    ```bash
    cd frontend
    npm install
    npm i -g @quasar/cli
    cp .env.example .env
    ```

3. **Instalar dependencias del backend (Symfony)**

    ```bash
    cd ..
    composer install
    cp .env.example .env
    ```

4. **Configuración de la base de datos**

    - Configura la conexión a la base de datos en el archivo `.env` del proyecto Symfony.
    - Ejecuta las migraciones para crear las tablas en la base de datos:

    ```bash
    php bin/console doctrine:migrations:migrate
    ```

## Ejecución

1. **Iniciar el servidor de desarrollo para Quasar (frontend)**

    ```bash
    cd frontend
    quasar dev
    ```

    La aplicación frontend estará disponible en `http://localhost:8080`.

2. **Iniciar el servidor de Symfony (backend)**

    ```bash
    cd ..
    symfony serve
    ```

    El servidor backend estará disponible en `http://localhost:8000`.

## Uso

- Accede a `http://localhost:8080` para interactuar con la aplicación Quasar (frontend).
- La API del backend estará disponible en `http://localhost:8000/api`.

## Contribución

Estoy atento a cualquier duda

## Licencia

Test para la posición de desarrollador web

