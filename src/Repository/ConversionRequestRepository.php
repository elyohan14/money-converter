<?php

namespace App\Repository;

use App\Entity\ConversionRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConversionRequest>
 *
 * @method ConversionRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConversionRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConversionRequest[]    findAll()
 * @method ConversionRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversionRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConversionRequest::class);
    }

    public function countSolicitudesPorIPYDia($ipAddress)
    {
        $startOfDay = new \DateTime('today');
        $endOfDay = new \DateTime('tomorrow');

        return $this->createQueryBuilder('cr')
            ->select('COUNT(cr.id)')
            ->where('cr.ipAddress = :ip')
            ->andWhere('cr.requestDate BETWEEN :start AND :end')
            ->setParameter('ip', $ipAddress)
            ->setParameter('start', $startOfDay)
            ->setParameter('end', $endOfDay)
            ->getQuery()
            ->getSingleScalarResult();
    }

//    /**
//     * @return ConversionRequest[] Returns an array of ConversionRequest objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ConversionRequest
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
