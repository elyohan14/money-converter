<?php

namespace App\Controller;

use App\Entity\ConversionRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Doctrine\ORM\EntityManagerInterface;

class ConverterController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    #[Route('/convert', name: 'app_converter')]
    public function convert(Request $request): JsonResponse
    {
        $ipAddress = $request->getClientIp();

        // Guardar la solicitud en la base de datos
        $conversionRequest = new ConversionRequest();
        $conversionRequest->setIpAddress($ipAddress);
        $conversionRequest->setRequestDate(new \DateTime());

        $this->entityManager->persist($conversionRequest);
        $this->entityManager->flush();

         // Verificar el límite de solicitudes para esta IP en el día actual
         $limit = 5;
         $solicitudesHoy = $this->entityManager->getRepository(ConversionRequest::class)
             ->countSolicitudesPorIPYDia($ipAddress);
 
         if ($solicitudesHoy > $limit) {
                // Retornar JSON con código de error
                return $this->json([
                    'error' => 'Se ha excedido el límite de solicitudes para hoy',
                    'status' => 429,
                ], 429);
         }

        $accessKey='a8a46bb3ae036ddb12c1c37663f1aacd';
        $amount = $request->query->get('amount');
        $from = $request->query->get('from');
        $to = $request->query->get('to');

        $client = HttpClient::create();
        $response = $client->request('GET', 'http://api.currencylayer.com/convert', [
            'query' => [
                'access_key' => $accessKey,
                'from' => $from,
                'to' => $to,
                'amount' => $amount
            ],
        ]);
        $statusCode = $response->getStatusCode();
        $content = $response->getContent();

        // Procesar la respuesta
        if ($statusCode === 200) {
            // Si la solicitud fue exitosa (código de estado 200)
            $data = json_decode($content, true);
            $data['query']['result'] = $data['result'];
            
            // Hacer algo con los datos recibidos
            // Por ejemplo, devolverlos como respuesta en JSON
            return $this->json($data['query']);
        } else {
            // Manejar otros códigos de estado si es necesario
            // return new Response('Error al obtener la tasa de conversión', $statusCode);
            return $this->json([
                'error' => 'Error al obtener la tasa de conversión',
                'status' => $statusCode,
            ]);
        }
    }
    #[Route('/conversion-requests', name: 'conversion_requests')]
    public function listConversionRequests(): JsonResponse
    {
        $conversionRequests = $this->entityManager
            ->getRepository(ConversionRequest::class)
            ->findAll(); // Obtener todas las solicitudes guardadas

        $requestsData = [];
        foreach ($conversionRequests as $request) {
            $requestsData[] = [
                'id' => $request->getId(),
                'ipAddress' => $request->getIpAddress(),
                'requestDate' => $request->getRequestDate()->format('Y-m-d H:i:s'),
            ];
        }

        return $this->json($requestsData);
    }
}
